
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

 require('./bootstrap');

//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });


$(function(){
  var totalOffset = 0;

  addAllListeners();

  function addAllListeners(offset = 0){
    totalOffset = totalOffset+offset;
    console.log(totalOffset);
    
    addCommentDeleteListeners();

    //preventing multiple listeners on the same element
    addVotingListeners(totalOffset);
    addSaveListeners(totalOffset);


  }
   /*
   *
   * Voting Ajax 
   * 
   */

  function addVotingListeners(offset=0){
    var upvoteElements = document.querySelectorAll(".upvote");
    var downvoteElements = document.querySelectorAll(".downvote");
    for (var i = offset; i < upvoteElements.length; i++) { //since upvote and downvote buttons are same
      upvoteElements[i].addEventListener("click", function() {
        vote(this.id);
      });
      downvoteElements[i].addEventListener("click", function() {
        vote(this.id);
      });
    }
  }

  function vote(id){
    if(AuthUser){
      var direction = id.split("_")[0];
      var post_id = id.split("_")[1];

      var url = "/vote/"+ direction + "/" + post_id;
      $.ajax({
        url: url,
        type: 'post',
        data: {
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        success: function (data) {
          if(data['result']['vote'] == 1){
            $("#" + id).addClass("upvoted");
            $("#" + "downvote_" + post_id).removeClass("downvoted");
          } else if(data['result']['vote'] == -1){
            $("#" + "upvote_" + post_id).removeClass("upvoted");
            $("#" + id).addClass("downvoted")
          } else {
            $("#" + "downvote_" + post_id).removeClass("downvoted");
            $("#" + "upvote_" + post_id).removeClass("upvoted");
          }
        }
      });
    } else {
      window.location.href="/login";
    }
  }

   /*
   *
   * saving Ajax 
   * 
   */

   function addSaveListeners(offset=0){
    var elements = document.querySelectorAll(".save");

    //first remove all previous event listners
    for (var i = offset; i < elements.length; i++) {
      elements[i].addEventListener("click", function() {
        save(this.id);
      });
    }
  }

  function save(id){
    if(AuthUser){
      var post_id = id.split("_")[1];

      var url = "/save/" + post_id;
      $.ajax({
        url: url,
        type: 'post',
        data: {
        },
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        dataType: 'json',
        success: function (data) {
          if(data['result']['saved'] == 1){
            $("#" + id).css('color', '#f44336');
            $("#" + id).find("i").removeClass("fa-heart-o");
            $("#" + id).find("i").addClass("fa-heart");
          } else if(data['result']['saved'] == 0){
            $("#" + id).css('color', '#bdbdbd');
            $("#" + id).find("i").removeClass("fa-heart");
            $("#" + id).find("i").addClass("fa-heart-o");
          }
        }
      });
    } else {
      window.location.href = "/login";
    }
  }
  /*
   *
   * Comment Delete Ajax 
   * 
   */

   function addCommentDeleteListeners(){
      var elements = document.querySelectorAll(".comment-delete");
      if(elements){
        for (var i = 0; i < elements.length; i++) {
          elements[i].addEventListener("click", function() {
            deleteComment(this.id);
          });
        }
      }
    }

  function deleteComment(id){
    var confirm = window.confirm("Are you sure, you want to delete this comment?");
    if (confirm == true) {
      $("#" + id).fadeToggle(100);
      if(AuthUser){
        var post_id = id.split("_")[1];

        var url = "/comment/delete/" + post_id;
        $.ajax({
          url: url,
          type: 'post',
          data: {
          },
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          dataType: 'json',
          success: function (data) {
            if(data['success']){
              $("#" + id).parent().fadeToggle(250);
            }
          }
        });
      } else {
        window.location.href = "/login";
      }
    }
  }


  /*
   *
   * Homepage Ajax 
   * 
   */
   $("#spinner").toggle();

   var delay_homepage_data_ajax = 1000;
   var processing = false;
   var currentPostCount = 20;
   var fetchPosts = 20;// please keep them same, otherwise change the function addeventlistners.

   $('#fetch-more').on('click', function(){
    $("#spinner").toggle(1000);
    if (processing == false) {
      processing = true;
      setTimeout(get_more_homepage_data, 1);
      processing = false;
    }

    function convertToSlug(Text){
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-');
    }

    function get_more_homepage_data(){
      var url = "/api/home/" + currentPostCount + "/" + fetchPosts + "/" + sort + "/" + category;
      $.get(url, function(data, status){
        $.each(data, function(key, post){
          var div = '<div class="row"> <div class="card">' +
          '<a href="/post/' + post["post_id"] + '/' + post["title"] +'">'+
          '            <img class="card-img-top" src="' 
          + post["image_url"] + 
          '" alt="Card image cap">' +
          '            <div class="card-block">' +
          '            <div class="card-title overflow-hidden"><strong>' 
          + post["title"] +
          '</strong></div>' +
          '            <div class="card-text overflow-hidden">'
          + post["text"].replace(/\|\|\|\|\|/g, ' ') +
          '</div>' +
          '            <div class="card-text post-meta">' +
          '            <small class="text-muted overflow-hidden">' +
          '            Last updated ' 
          + post['updatedAt'] +
          '            &nbsp;&nbsp;' +
          '            <i class="fa fa-chevron-up" aria-hidden="true"></i>' +
          '            &nbsp;' +
          + post['upvotesCount'] +
          '            &nbsp;&nbsp;' +
          '            <i class="fa fa-comments" aria-hidden="true"></i>' +
          '            &nbsp;' 
          + post['commentsCount'] +
          '             Comments' +
          '            </small></p>' +
          '            </div>' +
          '            </div></a>' +
          '            <div class="card-text post-source '
          + convertToSlug(post['source']) + 
                       '">' 
          + post['source'] +
                       '</div>'+
          '            <div class="bottom-bar">' +
          '            <div id="upvote_'
          + post["post_id"] +
          '" class="upvote">' +
          '            <i class="fa fa-chevron-up" aria-hidden="true"></i>' +
          '            </div>' +
          '            <div id="downvote_'
          + post["post_id"] +
          '" class="downvote">' +
          '            <i class="fa fa-chevron-down" aria-hidden="true"></i>' +
          '            </div>' +
          '            <div id="save_'
          + post["post_id"] +
          '" class="save">' +
          '            <i class="fa fa-heart-o" aria-hidden="true"></i>' +
          '            </div>' +
          '            </div>' +
          '            </div>' +
          '            </div>';

              //append the card into the parent container.
              $("#homepage-parent-container").append(div);
            }
            );
        currentPostCount += fetchPosts;
        $('#spinner').toggle(1000);
        addAllListeners(fetchPosts);
      });
    }
  });

 });


$(function() {

  $('#login-form-link').click(function(e) {
    $("#login-form").delay(100).fadeIn(100);
    $("#register-form").fadeOut(100);
    $('#register-form-link').removeClass('active');
    $(this).addClass('active');
    e.preventDefault();
  });
  $('#register-form-link').click(function(e) {
    $("#register-form").delay(100).fadeIn(100);
    $("#login-form").fadeOut(100);
    $('#login-form-link').removeClass('active');
    $(this).addClass('active');
    e.preventDefault();
  });

});

$(function(){
  $('#header-username').on('click', function(){
    $('#logout-row').fadeToggle(350);
    $('#get-comments-row').fadeToggle(350);
  });
  $('#sort-button').on('click', function(){
    $('#sort-options').fadeToggle(200);
    //$('html,body').animate({ scrollTop: 0 }, 'slow');
  });
  $('#categories-button').on('click', function(){
    $('#categories').fadeToggle(200);
    //$('html,body').animate({ scrollTop: 0 }, 'slow');
  });
});

$(function(){
  $('#add-comment-button').on('click', function(){
    if(AuthUser){
      $('#new-comment-input-div').toggle(250);
    } else{
      window.location.href="/login";
    }
  });

  $('#cancel-comment').on('click', function(){
    $('#new-comment-input-div').hide(250);
  });
});

$(function(){
  var colors = ['#0097A7', '#388E3C', '#303F9F', '#E64A19', '#455A64', '#283593', '#FFB74D', '#ef5350', '#E91E63'];
  $(".comment-username").each(function( index ) {
    var random_color = colors[Math.floor(Math.random() * colors.length)];
    $(this).css('color', random_color);
  });
});


$(function(){
  var lastScrollTop = 0;
  $(window).scroll(function(event){
    var st = $(this).scrollTop();
    if (st > lastScrollTop){
      $("#sort-options").fadeOut(200);
      $('#categories').fadeOut(200);
      $("#share-row").fadeOut(400);
    } else {
      $("#share-row").fadeIn(400);
    }
    lastScrollTop = st;
  });
});

