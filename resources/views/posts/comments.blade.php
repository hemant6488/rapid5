<div class="comments-container">
  @foreach ($comments as $comment)
  <div class="comment-row">
    <div class="comment-card">
      <div class="comment-username">
        {{ $comment->user->username }}:
      </div>
      <div class="comment-text">
        {{ $comment->comment }}
      </div>
      @if(array_key_exists('username', $data) and $comment->user->username == $data['username'])
        <div id="comment_{{ $comment->id }}" class="comment-delete">
          Delete
        </div>
      @endif
    </div>
  </div>
  @endforeach
</div>