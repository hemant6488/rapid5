<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta property="og:title" content="{{ trim($post->title) }}" />
  <meta property="og:description" content="
  @foreach (array_slice($data['sentences'], 0, 3) as $sentence)
    {{ $sentence }}
  @endforeach
  " />
  <link rel="shortcut icon" href="{{{ URL::asset('img/favicon.png') }}}">
  <meta property="og:image" content="{{ $post->image_url }}" />
  <link rel="stylesheet" href="{{ URL::asset('css/pages.css') }}">  
  <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
  <title> 
    Rapid5.in | {{ trim($post->title) }}
  </title>
  <script>
    var AuthUser = "{{{ (Auth::user()) ? Auth::user()->username : null }}}";
  </script>
  <!-- Google Tag Manager -->
  <script>
    if (document.location.hostname.search("rapid5.in") !== -1){
      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-5566LMZ');
    }
  </script>
  <!-- End Google Tag Manager -->

</head>
<body>
  <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5566LMZ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <div class="header-container">
    <div class="row header-row">
      <a href="/">
        <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 header-logo-full">
          <i>Rapid5<font size="1px">.in <font style="color: #4DB6AC"><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;beta</font></font></i>
        </div>
      </a>
      
      @if(array_key_exists('username', $data))
      <a href="{{ route('profilePage', ['username' => $data['username']]) }}">
        <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 header-profile">
          <div class="profile">
            <span>{{ strtoupper($data['username'][0]) }}</span>
          </div>
        </div>
      </a>
      @else
      <a href="/login">
        <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 header-profile">
          <div class="profile" style="padding-top: 5px; padding-left:5px;">
            <i class="fa fa-user-o" aria-hidden="true"></i>
          </div>
        </div>
      </a>
      @endif
    </div>
  </div>
  @yield('content')
  
  @yield('comments')

  <div id="share-row" class="row share-row">
    <div class="post-share-buttons">
      <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('post',['post_id' => $post->post_id, 'title' => str_slug($post->title, "-")]) }}" target="_blank">
        <img id="share-facebook-button" src="{{ asset('img/facebook.png') }}">
      </a>
      <a href="http://twitter.com/share?text={{ $post->title }}&url={{ route('post',['post_id' => $post->post_id, 'title' => str_slug($post->title, "-")]) }}&hashtags=rapid5" target="_blank">
        <img id="share-twitter-button" src="{{ asset('img/twitter.png') }}">
      </a>
      <a href="whatsapp://send?text=*{{ $post->title }}* %0A%0A{{ route('post',['post_id' => $post->post_id, 'title' => str_slug($post->title, "-")]) }}" data-action="share/whatsapp/share" target="_blank">
        <img id="share-whatsapp-button" src="{{ asset('img/whatsapp.png') }}">
      </a>
    </div>
  </div>
  <!-- Javascript -->
  <script src="{{ URL::asset('js/app.js') }}"></script>

</body>
</html>