@extends('posts.base')

@section('content')
<div id="homepage-parent-container" class="container-fluid" style="padding-top: 55px;">
  <?php $post = $data['post']; ?>
  <div class="row">
    <div class="card">
      <img class="card-img-top" src="{{ $post->image_url }}" alt="Card image cap">
      <div class="card-block">
        <div class="card-title"><strong>{{ $post->title }}</strong></div>
        <div class="card-text">
          @foreach($data['sentences'] as $sentence)
            <div><img src="{{ asset('img/rapid5_bullet_small.png') }}" height="10" style="width: 5%; margin: 0 4px 2px 0; display: inline;">{{$sentence}}</div>
          @endforeach
        </div>
        <div class="col-xs-5 col-sm-5 category-link">
          <a href={{ route('category', ['category'=>$post->categories->first()->name]) }}>
            <div>
              <i class="fa fa-arrow-left" aria-hidden="true" style="margin-bottom: 4px; font-size: 13px; vertical-align:middle"></i> &nbsp;&nbsp;{{ucwords($post->categories->first()->name)}}
            </div>
          </a>
        </div>
        <div class="col-xs-5 col-sm-5 source-link">
          <a href={{ $post->source_url }}>
            <div>
              Full Article&nbsp;&nbsp;<i class="fa fa-arrow-right" aria-hidden="true" style="margin-bottom: 4px; font-size: 13px; vertical-align:middle"></i> 
            </div>
          </a>
        </div>
        <div class="card-text post-meta margin-top-35px">
          <small class="text-muted">
            Updated {{ $post->updatedAt }}
            &nbsp;&nbsp;
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
            &nbsp;
            {{ $post->upvotesCount }}
            &nbsp;&nbsp;
            <i class="fa fa-comments" aria-hidden="true"></i>
            &nbsp;
            {{ $post->commentsCount }} Comments
          </small></p>
        </div>
      </div>
      @if($data['login'])
      <div class="bottom-bar">
        <div id="{{ 'upvote_' . $post->post_id }}" class="upvote{{ ($data['login'] and $data['is_voted_post'] == 1)? " upvoted":"" }}">
          <i class="fa fa-chevron-up" aria-hidden="true"></i>
        </div>
        <div id="{{ 'downvote_' . $post->post_id }}" class="downvote{{ ($data['login'] and $data['is_voted_post'] == -1)? " downvoted":"" }}">
          <i class="fa fa-chevron-down" aria-hidden="true"></i>
        </div>
        <div id="{{ 'save_' . $post->post_id }}" class="save{{ ($data['login'] and $data['is_saved_post']) ? " saved": ""}}">
          <i class="fa {{ ($data['login'] and $data['is_saved_post']) ? " fa-heart": " fa-heart-o"}}" aria-hidden="true"></i>
        </div>
      </div>
      @else
      <a href="/login">
        <div class="bottom-bar">
          <div class="upvote" upvoted":"" }}">
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
          </div>
          <div class="downvote" downvoted":"" }}">
            <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </div>
          <div class="save">
            <i class="fa fa-heart-o" aria-hidden="true"></i>
          </div>
        </div>
      </a>
      @endif
    </div>
  </div>
</div>
<div class="container-fluid">
  <form id="new-comment" action="/comment" method="POST">
    <div id="new-comment-input-div" class="row comment-add-div">
      <div class="card">
        <div>
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="post_id" value="{{ $post->post_id }}">
          <textarea form="new-comment" name="comment" placeholder="Be kind to strangers on the internet..."></textarea>
        </div>
        <div class="row comment-input-buttons">
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
            <button id="cancel-comment" type="button" class="btn btn-cancel">Cancel</button>
          </div>
          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6" style="float:right;">
            <button class="btn btn-submit" type="submit">Submit</button>
          </div>
        </div>
      </div>
    </div>
    @if($data['login'])
    <div class="row">
      <div class="card">
        <div id="add-comment-button" class="card-button add-comment-button">
          Comment
        </div>
      </div>
    </div>
    @else
    <a href="/login">
      <div class="row">
        <div class="card">
          <div id="add-comment-button-loggedout" class="card-button add-comment-button">
            Comment
          </div>
        </div>
      </div>
    </a>
    @endif
  </form>
</div>
@endsection

@section('comments')
@includeWhen(!is_null($data['post_comments']), 'posts.comments', ['comments' => $data['post_comments']])
@endsection