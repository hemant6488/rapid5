<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="shortcut icon" href="{{{ URL::asset('img/favicon.png') }}}">
  <title>Rapid5.in | Register or Login</title>
  
  <!-- Google Tag Manager -->
  <script>
    if (document.location.hostname.search("rapid5.in") !== -1){
      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-5566LMZ');
    }
  </script>
  <!-- End Google Tag Manager -->


  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

  <!-- Styles -->
  <link rel="stylesheet" href="{{ URL::asset('css/auth.css') }}">  
  <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
  {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
</head>
<body>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5566LMZ"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <div class="header-container">
    <div class="row header-row">
      <a href="/">
        <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10 header-logo-full limit-line-height">
            <i>Rapid5<font size="1px">.in <font style="color: #4DB6AC"><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;beta</font></font></i>
        </div>
      </a>
    </div>
  </div>
  @yield('content')

  <!-- JavaScripts -->
  <script src="{{ URL::asset('js/app.js') }}"></script>
  {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>