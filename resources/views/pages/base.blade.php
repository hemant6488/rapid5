<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ URL::asset('css/pages.css') }}">  
  <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
  <link rel="shortcut icon" href="{{{ URL::asset('img/favicon.png') }}}">
  <script>
    var AuthUser = "{{{ (Auth::user()) ? Auth::user()->username : null }}}";
    var category = "{{{ (array_key_exists('category', $data))? $data['category'] : null }}}"
    var sort = "{{ array_key_exists('sort', $data)? $data['sort'] : '' }}"
  </script>


  <!-- Google Tag Manager -->
  <script>
    if (document.location.hostname.search("rapid5.in") !== -1){
      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-5566LMZ');
    }
  </script>
  <!-- End Google Tag Manager -->

</head>
<body>
  <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5566LMZ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <div class="header-container">
    <div class="row header-row">


      <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 header-logo-full">
        <a href='/home'>
          <i>Rapid5<font size="1px">.in <font style="color: #4DB6AC"><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;beta</font></font></i>
        </a>
      </div>
      <div id="categories-button" class="col-md-5 col-lg-5 col-sm-5 col-xs-5 header-category" onClick="">
        <span>
          {{ array_key_exists('category', $data)? ucwords($data['category']) : 'Frontpage' }}
          &nbsp;&nbsp;
        </span>
        <i class="fa fa-sort" aria-hidden="true"></i>
      </div>


      <div id="sort-button" class="sort col-md-1 col-lg-1 col-sm-1 col-xs-1 center-block">
        <div class="">
          <i class="fa fa-sort-amount-desc" aria-hidden="true"></i>
        </div>
      </div>

      @if(array_key_exists('username', $data))
      <a href="{{ route('profilePage', ['username' => $data['username']]) }}">
        <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 header-profile">
          <div class="profile">
            <span>{{ strtoupper($data['username'][0]) }}</span>
          </div>
        </div>
      </a>
      @else
      <a href="/login">
        <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 header-profile">
          <div class="profile" style="padding-top: 5px; padding-left:5px;">
            <i class="fa fa-user-o" aria-hidden="true"></i>
          </div>
        </div>
      </a>
      @endif
    </div>
  
    <div class="col-xs-3" style="display: none;">
    </div>
    <div id="categories" class="col-xs-5 categories">
      <div class="" style="background-color: #fff; width: 100%">
        <a href="/home">
          <div class="row category-row">
            Frontpage
          </div>
        </a>
        @foreach($data['categories'] as $category)
        <a href="{{ route('category', ['category' => $category->name ])}}">
          <div class="row category-row">
            {{ ucwords($category->name) }}
          </div>
        </a>
        @endforeach
        <a href="{{ route('about') }}">
          <div class="row category-row" style="color:#90A4AE; border: 0; font-family: Verdana, Geneva, sans-serif; font-weight:normal; font-size: 12px; padding-bottom: 0;">
            About Us
          </div>
        </a>
      </div>
    </div>
    <div id="sort-options" class="col-xs-3 sort-options">
      <div class="" style="background-color: #fff; width: 100%;">
        <a href="{{array_key_exists('category', $data)? '/'.$data['category'].'/hot' : '/home/hot'}}">
          <div class="row sort-options-row">
            Hot
          </div>
        </a>
        <a href="{{array_key_exists('category', $data)? '/'.$data['category'].'/recent' : '/home/recent'}}">
          <div class="row sort-options-row">
            Recent
          </div>
        </a>
      </div>
    </div>
  </div>
  @yield('content')
  
  
  <!-- Javascript -->
  <script src="{{ URL::asset('js/app.js') }}"></script>
  
  <!-- Facebook SDK -->
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=1544872342258695';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  </script>
  <!-- End Facebook SDK -->
</body>
</html>