@extends('pages.base')

@section('content')
<?php $index = 0; $random_location = rand(2, 8); ?>
<div id="homepage-parent-container" class="container-fluid" style="padding-top: 55px;">
  @foreach ($data['posts'] as $post)
  <?php $index +=1; ?>
  <div class="row">
    <div class="card">
      <a href="{{ route('post',['post_id' => $post->post_id, 'title' => str_slug($post->title, "-")]) }}">
        <img class="card-img-top" src="{{ $post->image_url }}" alt="Card image cap">
        <div class="card-block">
          <div class="card-title overflow-hidden"><strong>{{ $post->title }}</strong></div>
          <div class="card-text overflow-hidden">
            {{ str_replace("|||||", " ", $post->text) }}
          </div>
          <div class="card-text post-meta">
            <small class="text-muted overflow-hidden">
              Updated {{ $post->updatedAt }}
              &nbsp;&nbsp;
              <i class="fa fa-chevron-up" aria-hidden="true"></i>
              &nbsp;
              <font style="{{$post->upvotesCount? 'color:F57C00;' : '' }}">
                {{ $post->upvotesCount }}
              </font>
              &nbsp;&nbsp;
              <i class="fa fa-comments" aria-hidden="true"></i>
              &nbsp;
              {{ $post->commentsCount }} Comments
            </small></p>
          </div>
        </div>
      </a>

      <div class="card-text post-source {{ str_slug($post->source) }}">
        {{ $post->source }}
      </div>
      @if($data['login'])
      <div class="bottom-bar">
        <div id="{{ 'upvote_' . $post->post_id }}" class="upvote{{ ($data['login'] and array_key_exists($post->post_id, $data['votes']) and $data['votes'][$post->post_id] == 1)? " upvoted":"" }}">
          <i class="fa fa-chevron-up" aria-hidden="true"></i>
        </div>
        <div id="{{ 'downvote_' . $post->post_id }}" class="downvote{{ ($data['login'] and array_key_exists($post->post_id, $data['votes']) and $data['votes'][$post->post_id] == -1)? " downvoted":"" }}">
          <i class="fa fa-chevron-down" aria-hidden="true"></i>
        </div>
        <div id="{{ 'save_' . $post->post_id }}" class="save{{ ($data['login'] and in_array($post->post_id, $data['saved_posts'])) ? " saved": ""}}">
          <i class="fa {{ ($data['login'] and in_array($post->post_id, $data['saved_posts'])) ? " fa-heart": " fa-heart-o"}}" aria-hidden="true"></i>
        </div>
      </div>
      @else
      <a href="/login">
        <div class="bottom-bar">
          <div class="upvote" upvoted":"" }}">
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
          </div>
          <div class="downvote" downvoted":"" }}">
            <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </div>
          <div class="save">
            <i class="fa fa-heart-o" aria-hidden="true"></i>
          </div>
        </div>
      </a>
      @endif
    </div>
  </div>
  @if($index == $random_location)
  <div class="row">
    <div class="card social-card">
      <div>
        <img src="{{ URL::asset('img/social-banner-' . rand(1,6) . '.jpg') }}"/>
      </div>
      <div class="social-card-heading">
        Follow Rapid5 for Regular Updates
      </div>
      <div class="social-card-body">
        <div class="fb-like" data-href="https://facebook.com/rapid5.in" data-width="200" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
        <div class="twitter-follow">
          <a href="https://twitter.com/rapid5_in?ref_src=twsrc%5Etfw" class="twitter-follow-button" data-show-screen-name="false" data-show-count="true">Follow @rapid5_in</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
      </div>
    </div>
  </div>
  @endif
  @endforeach
</div>
<div class="container-fluid">
  <div id="spinner" class="row">
    <div class="card">
      <div class="card-spinner">
        <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="card">
      <div class="card-button">
        <button id="fetch-more" type="button" class="btn btn-raised btn-primary btn-block"><i class="fa fa-caret-down" aria-hidden="true" style="margin-right: 3%;"></i>More</button>
      </div>
    </div>
  </div>
</div>
@endsection