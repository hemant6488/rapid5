@extends('pages.base')

@section('content')

<div class="container-fluid" style="padding-top: 55px;">
  <div class="row">
    <div class="card" style="padding: 5px; text-align: justify;">
      How you doing? Glad you want to know more about us. <br><br>
      <a href="/" style="color:#FF5722; font-family: Verdana, Geneva, sans-serif;">Rapid5.in</a> is a news aggregation site which provides an excerpt of a news article in 5 points. We use deep learning to categorize and summarize articles based on the content. So sometimes, things may not add up, and the excerpt can have discrepancies. <br><br>
      Sounds interesting? Want to join us in creating something amazing? or just have a query? <br>
      Shoot an email to:  <a href="mailto:contact@rapid5.in" style="color:#FF5722; font-family: Verdana, Geneva, sans-serif;">contact@rapid5.in</a>.
    </div>
  </div>
</div>

@endsection