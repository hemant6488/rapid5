<div class="row text-no-decoration" style="padding-top: 55px;">
  <a href="{{ route('userComments', ['username'=>$data['username']])}}">
    <div id="get-comments-row" class="card get-comments-row">
      Comments
      <i class="fa fa-comment-o" aria-hidden="true"></i>
    </div>
  </a>
</div>
<div class="row text-no-decoration">
  <a href="/logout">
    <div id="logout-row" class="card logout-row">
      Logout
      <i class="fa fa-frown-o" aria-hidden="true"></i>
    </div>
  </a>
</div>