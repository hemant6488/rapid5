@extends('profile.base')
@section('content')
  @include('profile.menu')
  <div class="profile-comments"  style="margin-top: 10px;">
  @if ($data['comments']->count() == 0 and !array_key_exists('otherUser', $data))
    <div class="comment-row">
      <div class="comment-card" style="font-size: 14px; padding:10px; text-align: center;">
        Your comments &nbsp;<i class="fa fa-comments" aria-hidden="true" style="color:#2196F3;"></i>&nbsp; show up here.
      </div>
    </div>
  @endif

  @if (array_key_exists('otherUser', $data))
    <div class="row" style="padding-bottom: 15px;">
      <div class="card saved-post-banner">
         @ {{ $data['otherUser'] }}'s comments.
      </div>
    </div>
  @endif
  
  @if ($data['comments']->count() == 0 and array_key_exists('otherUser', $data))
    <div class="comment-row">
      <div class="comment-card" style="font-size: 14px; padding:10px; text-align: center;">
        {{ $data['otherUser'] }} has no comments &nbsp;<i class="fa fa-comments" aria-hidden="true" style="color:#2196F3;"></i>&nbsp; yet.
      </div>
    </div>
  @endif

  @foreach ($data['comments'] as $comment)
  <div class="comment-row">
    <div class="comment-card">
      <div class="comment-username">
        {{ $comment->user->username }} >> {{ $comment->post->title }}
      </div>
      <a href="{{ route('post', ['post_id' => $comment->post->post_id, 'title' => $comment->post->title]) }} ">
        <div class="comment-text">
          {{ $comment->comment }}
        </div>
      </a>
      @if($comment->user->username == $data['username'])
      <div id="comment_{{ $comment->id }}" class="comment-delete">
        Delete
      </div>
      @endif
    </div>
  </div>
  @endforeach
</div>
@endsection