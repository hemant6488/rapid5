@extends('profile.base')

@section('content')
@include('profile.menu')

@if ($data['posts']->count() != 0)
  <div class="row">
    <div class="card saved-post-banner">
      Saved Posts
    </div>
  </div>
@endif

<div id="homepage-parent-container" class="container-fluid">
  @if ($data['posts']->count() == 0)
    <div class="row">
      <div class="card" style="text-align: center; padding: 10px;">
        The posts you &nbsp;<i class="fa fa-heart" aria-hidden="true" style="color:#f44336;"></i>&nbsp; show up here.
      </div>
    </div>
  @else
    @foreach ($data['posts'] as $post)
    <div class="row">
      <div class="card">
        <a href="{{ route('post',['post_id' => $post->post_id, 'title' => str_slug($post->title, "-")]) }}">
          <img class="card-img-top" src="{{ $post->image_url }}" alt="Card image cap">
          <div class="card-block">
            <div class="card-title overflow-hidden"><strong>{{ $post->title }}</strong></div>
            <div class="card-text overflow-hidden">
              {{ str_replace("|||||", " ", $post->text) }}
            </div>
            <div class="card-text post-meta">
              <small class="text-muted overflow-hidden">
                Updated {{ $post->updatedAt }}
                &nbsp;&nbsp;
                <i class="fa fa-chevron-up" aria-hidden="true"></i>
                &nbsp;
                {{ $post->upvotesCount }}
                &nbsp;&nbsp;
                <i class="fa fa-comments" aria-hidden="true"></i>
                &nbsp;
                {{ $post->commentsCount }} Comments
              </small></p>
            </div>
          </div>
        </a>
        <div class="card-text post-source {{ str_slug($post->source) }}">
            {{ $post->source }}
        </div>
        <div class="bottom-bar">
          <div id="{{ 'upvote_' . $post->post_id }}" class="upvote{{ (array_key_exists($post->post_id, $data['votes']) and $data['votes'][$post->post_id] == 1)? " upvoted":"" }}">
            <i class="fa fa-chevron-up" aria-hidden="true"></i>
          </div>
          <div id="{{ 'downvote_' . $post->post_id }}" class="downvote{{ (array_key_exists($post->post_id, $data['votes']) and $data['votes'][$post->post_id] == -1)? " downvoted":"" }}">
            <i class="fa fa-chevron-down" aria-hidden="true"></i>
          </div>
          <div id="{{ 'save_' . $post->post_id }}" class="save{{ (in_array($post->post_id, $data['saved_posts'])) ? " saved": ""}}">
            <i class="fa {{ (in_array($post->post_id, $data['saved_posts'])) ? " fa-heart": " fa-heart-o"}}" aria-hidden="true"></i>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  @endif
</div>
@endsection