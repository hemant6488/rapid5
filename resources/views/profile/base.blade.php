<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{ URL::asset('css/pages.css') }}">  
  <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
  <link rel="shortcut icon" href="{{{ URL::asset('img/favicon.png') }}}">
  <script>
    var AuthUser = "{{{ (Auth::user()) ? Auth::user()->username : null }}}";
  </script>
  <!-- Google Tag Manager -->
  <script>
    if (document.location.hostname.search("rapid5.in") !== -1){
      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-5566LMZ');
    }
  </script>
  <!-- End Google Tag Manager -->

</head>
<body>
  <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5566LMZ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->
  <div class="header-container">
    <div class="row header-row">
      
      <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 header-logo-full">
        <a href='/home'>
          <i>Rapid5<font size="1px">.in <font style="color: #4DB6AC"><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;beta</font></font></i>
        </a>
      </div>
      
      <div id="header-username" class="col-md-6 col-lg-6 col-sm-6 col-xs-6 header-username">
        <span>{{ "@".$data['username'] }}&nbsp;&nbsp;</span>
        <i class="fa fa-sort" aria-hidden="true"></i>
      </div>

      <a href="{{ route('profilePage', ['username' => $data['username']]) }}">
        <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1 header-profile">
          <div class="profile">
            <span>{{ strtoupper($data['username'][0]) }}</span>
          </div>
        </div>
      </a>
    </div>
  </div>
  @yield('content')
  
  
  <!-- Javascript -->
  <script src="{{ URL::asset('js/app.js') }}"></script>

</body>
</html>