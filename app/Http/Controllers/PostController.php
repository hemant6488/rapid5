<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Cache;

class PostController extends Controller
{ 
    private $POST_CACHE_TIMEOUT = 8320;// 7 days, i mean, posts don't really change.

    public function isSavedPost($user, $post_id){
      $saved_posts = $user->savedPosts()->where('saved', 1)->pluck('post_id')->toArray();
      if(in_array($post_id, $saved_posts)){
        return true;
      } else {
        return false;
      }
    }

    public function isVotedPost($user, $post_id){
      $voted_posts = $user->votes()->whereNotIn('vote', [0])->pluck('vote', 'post_id')->toArray();
      if(array_key_exists($post_id, $voted_posts)){
        return $voted_posts[$post_id];
      } else {
        return false;
      }
    }

    public function prettyPostText($text){
      $sentences = array_filter(explode("|||||", $text));

      //remove unpaired quotes
      foreach($sentences as $key => $sentence){
        if (substr_count($sentences[$key], '“') != substr_count($sentences[$key], '”')){
          $sentences[$key] = str_replace("”", "", str_replace("“", "", $sentences[$key]));
        } 
        if (substr_count($sentences[$key], '"')%2 != 0){
          $sentences[$key] = str_replace('"', '', $sentences[$key]);
        }
        if (substr_count($sentences[$key], "'")%2 != 0){
          $sentences[$key] = str_replace("'", "", $sentences[$key]);
        }
      }
      return $sentences;
    }

    public function index($post_id){
      $data['post'] = Cache::remember('post_'.$post_id, $this->POST_CACHE_TIMEOUT, function () use ($post_id){
                        return Post::find($post_id);
                      });
      $data['login'] = false;
      $data['post_comments'] = Cache::remember('post_comments_'.$post_id, $this->POST_CACHE_TIMEOUT, function() use ($data) {
                                    return $data['post']->comments()->where('soft_delete', '0')->orderByDesc('created_at')->with('user')->get();
                                });
      $data['sentences'] = Cache::remember('post_sentences_'.$post_id, $this->POST_CACHE_TIMEOUT, function () use ($data){
                                return $this->prettyPostText($data['post']['text']);
                              });

      if(Auth::check()){
        $user = Auth::user();
        $data['is_saved_post'] = $this->isSavedPost($user, $post_id);
        $data['is_voted_post'] = $this->isVotedPost($user, $post_id);
        $data['login'] = true;
        $data['username'] = $user->username;
      }
      return view('posts.index', ['data' => $data]);
    }
}
