<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Post;
use App\User;
use App\Category;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
  private $POST_COUNT = 20;
  private $RECENT_DAYS = 3;
  private $CATEGORY_CACHE_TIMEOUT = 360; //6 hours
  private $CATEGORY_POSTS_CACHE_TIMEOUT = 1; //cache for category model, which is used to retreive posts under that category.
  private $POSTS_LIST_CACHE_TIMEOUT = 5; //cache posts lists
  private $VOTES_TIMEOUT = 20;

  public function index($category_name="frontpage", $sort = "recent"){
    $data['login'] = false;
    #$data['categories'] = Category::all(); //for categories list on topbar
    $data['categories'] = Cache::remember('all_categories', $this->CATEGORY_CACHE_TIMEOUT, function () {
                              return Category::all();
                          });
    $data['sort'] = $sort;
    $category = null;

    if(Auth::check()){
      $user = Auth::user();
      $data['saved_posts'] = $user->savedPosts()->where('saved', 1)->pluck('post_id')->toArray();
      $data['votes'] = Cache::remember('votes_'.$user->username, $this->VOTES_TIMEOUT, function() use ($user) {
                          return $user->votes()->whereNotIn('vote', [0])->pluck('vote', 'post_id')->toArray();
                        });
      $data['login'] = true;
      $data['username'] = $user->username;
    }
    #$category = Category::where('name', $category_name)->first();
    if($category_name != "frontpage"){
        $category = Cache::remember('category_'.$category_name, $this->CATEGORY_POSTS_CACHE_TIMEOUT, function() use ($category_name){
                    return Category::where('name', $category_name)->first();
                });
    }

    if($category_name == "frontpage" or $category == null){
      if($sort == "hot"){
        $data['posts'] = Cache::remember('frontpage_hot_posts', $this->POSTS_LIST_CACHE_TIMEOUT, function(){
                            $date = new Carbon;
                            $date->subDays($this->RECENT_DAYS);
                            return Post::where('created_at', '>', $date->toDateTimeString())->get()->sortByDesc('score')->take($this->POST_COUNT);   
                          });
      } else { //recent
        $data['posts'] = Cache::remember('frontpage_recent_posts', $this->POSTS_LIST_CACHE_TIMEOUT, function(){
                            return Post::orderBy('created_at', 'desc')->take($this->POST_COUNT)->get();
                          });
      }      
    } else {
      $data['category'] = $category->name;
      if($sort == "hot"){
        $data['posts'] = Cache::remember($category->name . '_hot_posts', $this->POSTS_LIST_CACHE_TIMEOUT, function() use ($category){
                            $date = new Carbon;
                            $date->subDays($this->RECENT_DAYS);
                            
                            return $category->posts->where('created_at', '>', $date->toDateTimeString())->sortByDesc('score')->take($this->POST_COUNT);
                          });
      } else { //recent
        #$data['posts'] = $category->recentPosts->take($this->POST_COUNT);
        $data['posts'] = Cache::remember($category->name . '_recent_posts', $this->POSTS_LIST_CACHE_TIMEOUT, function() use ($category){
                            return $category->recentPosts->take($this->POST_COUNT);
                          });
      }
    }

    return view('pages.home', ['data' => $data]);
  }

  public function homepageData($offset, $limit, $sort = "recent", $category_name = "frontpage"){
    $category = null;

    if($category_name != "frontpage"){
        $category = Cache::remember('category_'.$category_name, $this->CATEGORY_POSTS_CACHE_TIMEOUT, function() use ($category_name){
                    return Category::where('name', $category_name)->first();
                });
    }
    
    if($category_name == "frontpage" or $category == null){
      if($sort == "hot"){
        $posts = Cache::remember($offset.'_'.$limit.'_frontpage_hot_posts', $this->POSTS_LIST_CACHE_TIMEOUT, function() use ($offset, $limit){
                    $date = new Carbon;
                    $date->subDays($this->RECENT_DAYS);
                   
                    return Post::where('created_at', '>', $date->toDateTimeString())->get()->sortByDesc('score')->slice($offset)->take($limit);
                  });
      } else {
        $posts = Cache::remember($offset.'_'.$limit.'_frontpage_recent_posts', $this->POSTS_LIST_CACHE_TIMEOUT, function() use ($offset, $limit){
                    return Post::orderBy('created_at', 'desc')->skip($offset)->take($limit)->get();
                  });
      }
    } else {
      if($sort == "hot"){
        $posts = Cache::remember($offset.'_'.$limit.'_'.$category->name . '_hot_posts', $this->POSTS_LIST_CACHE_TIMEOUT, function() use ($category, $offset, $limit){
                            $date = new Carbon;
                            $date->subDays($this->RECENT_DAYS);

                            return $category->posts->where('created_at', '>', $date->toDateTimeString())->sortByDesc('score')->slice($offset)->take($limit);
                });
      } else {
        $posts = Cache::remember($offset.'_'.$limit.'_'.$category->name . '_recent_posts', $this->POSTS_LIST_CACHE_TIMEOUT, function() use ($category, $offset, $limit){
                    return $category->recentPosts()->skip($offset)->take($limit)->get();
                  });
      }
    }
    return $posts;
  }

  public function about(){
    $data['login'] = false;
    $data['categories'] = Cache::remember('all_categories', $this->CATEGORY_CACHE_TIMEOUT, function () {
                              return Category::all();
                          });
    $data['category'] = "About Us";
    
    if(Auth::check()){
      $user = Auth::user();
      $data['login'] = true;
      $data['username'] = $user->username;
    }

    return view('pages.about', ['data' => $data]);
  }
}
