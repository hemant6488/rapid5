<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Vote;
use App\Post;
use AuthenticatesUsers;
use Illuminate\Support\Facades\Cache;

class UserController extends Controller
{
  private $SAVED_POST_CACHE_TIMEOUT = 8320;// 7 days, i mean, saved posts don't really change, and when they do, we clear caches.
  private $VOTES_TIMEOUT = 20;
  private $COMMENTS_TIMEOUT = 20;

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index($username){
    $user = Auth::user();
    Cache::flush();
    if($username == $user->username){
      $data['saved_posts'] = Cache::remember('saved_posts_ids_'.$user->username, $this->SAVED_POST_CACHE_TIMEOUT, function() use ($user) {
                                return $user->savedPosts()->where('saved', 1)->pluck('post_id')->toArray();
                              });
      $data['posts'] = Cache::remember('saved_posts_'.$user->username, $this->SAVED_POST_CACHE_TIMEOUT, function() use ($data) {
                            return Post::orderBy('created_at', 'desc')->findMany($data['saved_posts']);
                        });
      $data['votes'] = Cache::remember('votes_'.$user->username, $this->VOTES_TIMEOUT, function() use ($user) {
                            return $user->votes()->whereNotIn('vote', [0])->pluck('vote', 'post_id')->toArray();
                        });
      $data['username'] = $user->username;
      return view('profile.index', ['data' => $data]);
    } else {
      return $this->getOtherUserComments($username);
    }
  }

  public function getUserComments(){
    $user = Auth::user();
    $data['comments'] = Cache::remember('comments_'.$user->username, $this->COMMENTS_TIMEOUT, function() use ($user) {
                            return $user->comments()->where('soft_delete', 0)->orderBy('created_at', 'desc')->get();
                        });
    $data['username'] = $user->username;
    return view('profile.comments', ['data' => $data]);
  }

  public function getOtherUserComments($username){
    $user = Auth::user();
    $otherUser = User::where('username', $username)->firstOrFail();
    $data['comments'] = Cache::remember('comments_'.$otherUser->username, $this->COMMENTS_TIMEOUT, function() use ($otherUser) {
                            return $otherUser->comments()->where('soft_delete', 0)->orderBy('created_at', 'desc')->get();
                        });
    $data['username'] = $user->username;
    $data['otherUser'] = $otherUser->username;
    return view('profile.comments', ['data' => $data]);
  }

  public function logout(Request $request){
    Auth::logout();
    return redirect()->route('home');
  }
}
