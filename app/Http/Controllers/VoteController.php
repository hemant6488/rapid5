<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Vote;
use App\User;
use Illuminate\Support\Facades\Cache;


class VoteController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }


  public function vote($direction, $post_id){

    $user = Auth::user();
    $user_id = Auth::id();
    $vote = User::find($user_id)->votes()->where('post_id', $post_id)->first();

    Cache::forget('votes_'.$user->username);
    $data['success'] = false;
    if(is_null($vote)){ //no vote has been created for post_id and user_id
      if($direction == "upvote"){
        $data['result'] = Vote::create([
          'post_id' => $post_id,
          'user_id' => $user_id,
          'vote' => 1
        ]);
        $data['success'] = true;
        return $data;
      } else if($direction == "downvote"){
        $data['result'] = Vote::create([
          'post_id' => $post_id,
          'user_id' => $user_id,
          'vote' => -1
        ]);
        $data['success'] = true;
        return $data;
      }
    } else {
      if($vote->vote == 1){
        if($direction == "upvote"){
          $vote->vote = 0;
          $data['success'] = $vote->save();
          $data['result'] = $vote;
          return $data;
        } else if ($direction == "downvote"){
          $vote->vote = -1;
          $data['success'] = $vote->save();
          $data['result'] = $vote;
          return $data;
        }

      } else if ($vote->vote == -1){
        if($direction == "upvote"){
          $vote->vote = 1;
          $data['success'] = $vote->save();
          $data['result'] = $vote;
          return $data;
        } else if ($direction == "downvote"){
          $vote->vote = 0;
          $data['success'] = $vote->save();
          $data['result'] = $vote;
          return $data;
        }
      } else {
        if($direction == "upvote"){
          $vote->vote = 1;
          $data['success'] = $vote->save();
          $data['result'] = $vote;
          return $data;
        } else if ($direction == "downvote"){
          $vote->vote = -1;
          $data['success'] = $vote->save();
          $data['result'] = $vote;
          return $data;
        }
      }
    }
    

  }
}
