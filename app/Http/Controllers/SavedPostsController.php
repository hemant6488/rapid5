<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\SavedPost;
use App\User;
use Illuminate\Support\Facades\Cache;


class SavedPostsController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function save($post_id){
    $user_id = Auth::id();
    $user = Auth::user();
    $saved_post = User::find($user_id)->savedPosts()->where('post_id', $post_id)->first();

    $data['success'] = false;
    if(is_null($saved_post)){ //means user is trying to save post for first time
      $data['result'] = SavedPost::create([
        'post_id' => $post_id,
        'user_id' => $user_id,
        'saved' => 1
      ]);
      $data['success'] = true;
      Cache::forget('saved_posts_ids_'.$user->username);
      Cache::forget('saved_posts_'.$user->username);
      return $data;
    } else {
      if($saved_post->saved == 1){
        $saved_post->saved = 0;
        $data['success'] = $saved_post->save(); 
        $data['result'] = $saved_post;
        Cache::forget('saved_posts_ids_'.$user->username);
        Cache::forget('saved_posts_'.$user->username);
        return $data;
      } else if($saved_post->saved == 0){
        $saved_post->saved = 1;
        $data['success'] = $saved_post->save(); 
        $data['result'] = $saved_post;
        Cache::forget('saved_posts_ids_'.$user->username);
        Cache::forget('saved_posts_'.$user->username);
        return $data;
      }
    }
  }
}
