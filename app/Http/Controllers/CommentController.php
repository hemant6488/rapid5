<?php

namespace App\Http\Controllers;

use Request;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Cache;


class CommentController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request){
    $input = Request::all();
    $user = Auth::user();

    $comment = new Comment;

    $comment->user_id = $user->id;
    $comment->post_id = $input['post_id'];
    $comment->comment = $input['comment'];
    $comment->user_ip = Request::ip();

    $comment->save();
    Cache::forget('post_comments_'.$input['post_id']);
    Cache::forget('comments_'.$user->username);
    return redirect()->back();
  }

  public function delete($comment_id){
    $comment = Comment::find($comment_id);
    $user = Auth::user();
    
    if($comment){
      $comment->user_ip = $comment->user_ip . ',' . Request::ip();
      $comment->soft_delete=1;
      $comment->save();
      $data['success'] = true;
      Cache::forget('post_comments_'.$comment->post_id);
      Cache::forget('comments_'.$user->username);
      return $data;
    } else{
      $data['success'] = false;
      $data['message'] = "No comment found.";
      return $data;
    }
  }
}
