<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedPost extends Model
{
  protected $primaryKey = 'saved_id';

  protected $fillable = [
    'post_id', 'user_id', 'saved',
  ];

  public function user(){
    return $this->hasOne('App\User', 'id', 'user_id');
  }

  public function post(){
    return $this->belongsTo('App\Post');
  }
}
