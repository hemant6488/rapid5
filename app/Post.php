<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
  protected $primaryKey = 'post_id';
  protected $appends = ['upvotesCount', 'commentsCount', 'updatedAt', 'score', 'source'];
  protected $dates = [
        'created_at',
        'updated_at'];
  
  public function votes(){
    return $this->hasMany('App\Vote');
  }

  public function getUpvotesCountAttribute(){
    return $this->hasMany('App\Vote', 'post_id')->where('vote', 1)->count();
  }

  public function getCommentsCountAttribute(){
    return $this->hasMany('App\Comment', 'post_id')->where('soft_delete', 0)->count();
  }

  public function comments(){
    return $this->hasMany('App\Comment', 'post_id');
  }

  public function saves(){
    return $this->hasMany('App\SavedPost');
  }

  public function getUpdatedAtAttribute(){
    if(!is_null($this->attributes['updated_at'])){
      return Carbon::parse($this->attributes['updated_at'])->diffForHumans();
    } else if(!is_null($this->attributes['created_at'])){
      return Carbon::parse($this->attributes['created_at'])->diffForHumans();
    } else {
      return "a while ago";
    }
  }

  public function getScoreAttribute(){
    $upvotes = $this->hasMany('App\Vote', 'post_id')->where('vote', 1)->count();
    $comments = $this->hasMany('App\Comment', 'post_id')->where('soft_delete', 0)->count();
    $saves = $this->hasMany('App\SavedPost', 'post_id')->count();

    $comment_weight = 10;
    $upvote_weight = 5;
    $save_weight = 15;

    return (($upvotes * $upvote_weight) + ($comments * $comment_weight) + ($saves * $save_weight));
  }

  public function getSourceAttribute(){
    if(strpos($this->source_url, 'hindustantimes.com') !== false){
      return "Hindustan Times";
    } else if(strpos($this->source_url, 'gadgets.ndtv.com') !== false){
      return "NDTV Gadgets360";
    } else if(strpos($this->source_url, 'ndtv.com') !== false){
      return "NDTV News";
    }


  }

  public function categories(){
    return $this->belongsToMany('App\Category', 'category_post', 'post_id', 'category_id');
  }
}