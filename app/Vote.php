<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
  protected $primaryKey = 'vote_id';

  protected $fillable = [
    'post_id', 'user_id', 'vote',
  ];

  public function user(){
    return $this->belongsTo('App\User');
  }

  public function post(){
    return $this->belongsTo('App\Post');
  }
}
