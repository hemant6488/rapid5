<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function votes(){
        return $this->hasMany('App\Vote');
    }

    public function savedPosts(){
        return $this->hasMany('App\SavedPost');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }
}
