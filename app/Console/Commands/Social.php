<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Post;
use App\User;
use Illuminate\Support\Facades\Log;
require_once __DIR__ . '/../../../vendor/facebook/graph-sdk/src/Facebook/autoload.php';
require_once __DIR__ . '/../../../vendor/codebird-php/src/codebird.php';

class Social extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Social:share';
    protected $RECENT_TIME = 15;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function tweet($link, $text, $image){
        \Codebird\Codebird::setConsumerKey("LJa8aDP0uLGyWl6suq1L907SI", "OR4uoxYn3hxohNGrfOvdGoss1Ntkia0is1gnzpexkCQC7WqSZY");
        $cb = \Codebird\Codebird::getInstance();
        $cb->setToken("938019203625316353-XKQqXZ15KM2QSR7iZ4dmVV8SV75AbQX", "LwSJxblkr88tf9nhpv81kY4q95Lyr5T3DZyjoNf2IBMx4");
         
        $params = array(
          'status' => $text . ' | Read More: ' . $link . ' #Rapid5',
          'media[]' => $image
        );
        $reply = $cb->statuses_updateWithMedia($params);
        Log::info("Tweeted: " . $text . " | Image: " . $image);
    }

    public function postToFacebook($link, $text){
        $shareData = array('link' => $link, 'message' => $text);

        $pageAccessToken = "EAAV9DZA8vBAcBALAW8uj3I264UCTWZCqKZC7XpYJi69jXlHsdAc2JLPCnTLV3qcXSpnz4YlbY1MLxQZAlCSCzqjTTRxp6TXmb18i4PZBbQ9JGgi0vfBPbMzTk5vvZBINgZBhMjj8i6ZCX0r1Y1U76b3iUh3oKvVLCldhJzc3zJAWEAZDZD";

        $fb = new \Facebook\Facebook([
             'app_id' => '1544872342258695',
             'app_secret' => 'daa62780e35d8db09955a3811a86db95',
             'default_graph_version' => 'v2.10',
            ]);


        try {
          $response = $fb->post('2064527547126206/feed/', $shareData, $pageAccessToken);
          Log::info("Posting to facebook: ". $response->getRequest()->getParams()['message'] . ' | link: ' . $response->getRequest()->getParams()['link']);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          Log::error('Graph returned an error: ' . $e->getMessage());
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          Log::error('Facebook SDK returned an error: ' . $e->getMessage());
          exit;
        }
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        $admin = User::where('username', 'hemant')->firstOrFail();
        $date = new Carbon;
        $date->subMinutes($this->RECENT_TIME);
        $recentlyVotedPost = $admin->votes()->where('vote', '1')->where('created_at', '>', $date->toDateTimeString())->orderBy('created_at', 'desc')->first()->post_id;
        $post = Post::find($recentlyVotedPost);

        $post_url = route('post', ['post_id' => $post->post_id, 'title' => urlencode($post->title)]);
        

        //Tweet
        $this->tweet($post_url, $post->title, $post->image_url);

        //Facebook Post
        $this->postToFacebook($post_url, $post->title);
    }
}
