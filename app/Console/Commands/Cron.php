<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\UnpublishedPost;
use App\Post;
use App\Category;
use Carbon\Carbon;


class Cron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Cron:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publishes unpublished posts.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $RECENT_HOURS = 6;
        $date = new Carbon;
        $date->subHours($RECENT_HOURS);
        
        $unpublished_posts = UnpublishedPost::where('created_at', '>', $date->toDateTimeString())->where('published', 0)->inRandomOrder()->get();

        foreach ($unpublished_posts as $unpublished_post){
            if($unpublished_post->image_url != null){
                $post = new Post;
                $post->title = stripslashes($unpublished_post->title);
                $post->text = stripslashes($unpublished_post->text);
                $post->source_url = $unpublished_post->source_url;
                $post->image_url = $unpublished_post->image_url;
                $post->save();

                $unpublished_post->published = 1;
                $unpublished_post->save();

                //categorizing
                $categories = explode(',', $unpublished_post->categories);
                foreach($categories as $category_name){
                    $category = Category::where('name',$category_name)->get();
                    try {
                        if($category_name == "technology"){ #temporary workaround since too many wrong tech posts
                            if($categories[0] == "technology"){
                                $post->categories()->attach($category);         
                            }
                        } else {
                            $post->categories()->attach($category);
                        }
                    } catch (\Illuminate\Database\QueryException $e) {
                        var_dump($e->errorInfo );
                    }
                }
            }
        }

    }
}
