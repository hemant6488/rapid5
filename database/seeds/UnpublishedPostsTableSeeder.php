<?php

use Illuminate\Database\Seeder;

class UnpublishedPostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $categories = ['business','entertainment','india','lifestyle','politics','sports','technology','world'];
        for($i = 0; $i < 200; $i++ ){
          DB::table('unpublished_posts')->insert([
            'title' => str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)),
            'text' => str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)),
            'image_url' => 'https://i.ytimg.com/vi/kLAnVd5y7M4/maxresdefault.jpg',
            'source_url' => 'http://fontawesome.io/cheatsheet/',
            'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'published' => 0,
            'categories' => implode(",", Array($categories[array_rand($categories)], $categories[array_rand($categories)])),
        ]);
        }
    }
}
