<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 5000; $i++ ){
          DB::table('comments')->insert([
            'comment' => str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)),
            'post_id' => rand(1,80),
            'parent_id' => ($i==0? 0: $i-1),
            'user_id' => rand(1,2),
        ]);
        }
    }
}
