<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'business',
        ]);
        DB::table('categories')->insert([
            'name' => 'entertainment',
        ]);
        DB::table('categories')->insert([
            'name' => 'india',
        ]);
        DB::table('categories')->insert([
            'name' => 'lifestyle',
        ]);
        DB::table('categories')->insert([
            'name' => 'politics',
        ]);
        DB::table('categories')->insert([
            'name' => 'sports',
        ]);
        DB::table('categories')->insert([
            'name' => 'technology',
        ]);
        DB::table('categories')->insert([
            'name' => 'world',
        ]);
        
    }
}
