<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 200; $i++ ){
          DB::table('posts')->insert([
            'title' => str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)),
            'text' => str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)) . ' ' . str_random(rand(1, 10)),
            'image_url' => 'https://i.ytimg.com/vi/kLAnVd5y7M4/maxresdefault.jpg',
            'source_url' => 'http://fontawesome.io/cheatsheet/',
            'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        }
    }
}
