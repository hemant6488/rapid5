<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');

Auth::routes();

//Errors
Route::get('error', [
    'as' => 'errors.defaultError',
    'uses' => 'ErrorController@defaultError' ]);

/*
 Voting Routes
*/

Route::post('/vote/{direction}/{post_id}', 'VoteController@vote')->name('vote');
Route::post('/save/{post_id}', 'SavedPostsController@save')->name('save');

/*
  Profile Routes
*/

Route::get('/user/{username}', 'UserController@index')->name('profilePage');
Route::get('/user/{username}/comments', 'UserController@getUserComments')->name('userComments');
Route::get('/logout', 'UserController@logout');

/*
  Post Routes
*/
Route::get('/post/{post_id}', 'PostController@index')->name('post_only_id');
Route::get('/post/{post_id}/{title}', 'PostController@index')->name('post');

/*
 Comment Routes
*/

Route::post('/comment', 'CommentController@index')->name('comment');
Route::post('/comment/delete/{comment_id}', 'CommentController@delete')->name('comment');

Route::get('/{category}/{sort?}', 'HomeController@index')->name('category');
